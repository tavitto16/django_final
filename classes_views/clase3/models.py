from django.db import models
from django.urls import reverse
# Create your models here.

class Escuela(models.Model):
	name = models.CharField(max_length=128)
	principal = models.CharField(max_length=128)
	location =  models.CharField(max_length=128)

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return reverse("clase3_app:detalle",kwargs={'pk':self.pk})

class Estudiante(models.Model):
	name = models.CharField(max_length=256)
	age = models.PositiveIntegerField()
	school = models.ForeignKey(Escuela,on_delete=models.CASCADE,related_name = 'students')

	def __str__(self):
		return self.name
