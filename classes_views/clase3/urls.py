from django.conf.urls import url
from clase3 import views
from django.urls import path

app_name = 'clase3_app'

urlpatterns = [
	url('index',views.EscuelaListView.as_view(),name='list'),
	#url(r'^(?P<pk>[-\w]+)/$',views.EscuelaDetailView.as_view(),name='detalle'),
	path('<int:pk>/',views.EscuelaDetailView.as_view(),name='detalle'),
	path('create/',views.EscuelaCreateView.as_view(),name="create"),
	path('update/<int:pk>',views.EscuelaUpdateView.as_view(),name="update"),
	path('delete/<int:pk>',views.EscuelaDeleteView.as_view(),name="delete"),
]
#pk primary key of the school 
# co