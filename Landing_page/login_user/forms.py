from django import forms
from login_user.models import User

class NewUserForm(forms.ModelForm):
	class Meta():
		model = User
		fields = "__all__"