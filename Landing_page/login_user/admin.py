from django.contrib import admin

# Register your models here.
from login_user.models import User

admin.site.register(User)