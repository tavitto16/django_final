from django.shortcuts import render
from django.views.generic import View,TemplateView, ListView, DetailView,CreateView,UpdateView,DeleteView
from django.urls import reverse, reverse_lazy 
from . import models

# Create your views here.

class IndexView(TemplateView):
	template_name = 'clase3/index.html'

class EscuelaListView(ListView):
	# listView por defecto devuelve el nombre del objeto, en minusculas y agrega al final un _list
	#en este caso seria escuela_list
	#se puede cambiar, para poner otro nombre usando context_object_name
	context_object_name = 'escuelas'
	model  = models.Escuela
	template_name = 'clase3/escualeas_list.html'
	

class EscuelaDetailView(DetailView):
	model = models.Escuela
	# def get_context_data(self,**kwargs):
	# 	print("entra")
	# 	context = super().get_context_data(**kwargs)
	# 	#context = super().get_object_or_404(pk=kwargs['pk'])
	# 	context['pk_1'] = self.kwargs['pk']
	# 	return context

	context_object_name = "escuela_detalle"

	template_name = 'clase3/escuela_detail.html'

class EscuelaCreateView(CreateView):
	#recodar que por defecto context_object_name se hace con el nombre del model en minuscula
	# en este caso escuela
	# si se quiere cambiar se ha de usar la variable context_object_name = "loquesdea"
	#para poder llamarlo desde algun html (o el por defecto o cambiarlo con template_name)
	fields = ('name','principal','location')
	model = models.Escuela

class EscuelaUpdateView(UpdateView):
	#recodar que por defecto context_object_name se hace con el nombre del model en minuscula
	# en este caso escuela
	# si se quiere cambiar se ha de usar la variable context_object_name = "loquesdea"
	#para poder llamarlo desde algun html (o el por defecto o cambiarlo con template_name)
	
	fields = ('name','principal')
	model = models.Escuela

class EscuelaDeleteView(DeleteView):
	model = models.Escuela
	#recodar que por defecto context_object_name se hace con el nombre del model en minuscula
	# en este caso escuela
	# si se quiere cambiar se ha de usar la variable context_object_name = "loquesdea"
	#para poder llamarlo desde el html list
	#reverse_lazy te entregara la interface list con los valores actualisdos(sin el que has borrado)
	success_url = reverse_lazy('clase3_app:list')
