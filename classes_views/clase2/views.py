from django.shortcuts import render
from django.views.generic import View,TemplateView
# Create your views here.


class IndexView(TemplateView):
	template_name = 'clase2/index.html'
	# hay 2 tipos. el primero es *args, donde solo se inserta el valor (tuplas)    
	# kwargs donde se insertan de modo diccionario, pueden ser cuantos valores como se desee
	def get_context_data(self,**kwargs):
		context = super().get_context_data(**kwargs)
		context['injectme'] = 'BASIC INJECTION'
		return context

