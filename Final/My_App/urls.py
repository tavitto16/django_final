from django.conf.urls import url
from My_App import views

#TEMPLATE URLS

app_name='My_App'

urlpatterns = [
	url('register/', views.register,name='register'),
	url('user_login/', views.user_login,name='user_login'),
]