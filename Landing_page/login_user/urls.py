from django.conf.urls import url
from login_user import views

urlpatterns = [
    url('index/',views.index,name='first'),
    url('login/',views.login_User,name="login"),
    url("users/",views.users,name="users"),
]
