from django import forms
from django.core import validators



#3 ) crear funciones con validators  ** mas recomendada
#ejemplo  checkear que comienza el nombre por z

def check_for_z(value):
	if value[0].lower() != 'z':
		raise forms.ValidationError("El nombre no empieza por z")



class MyFormulario(forms.Form):
	#name = forms.CharField(validators =[check_for_z])
	name = forms.CharField()
	email = forms.EmailField()
	verify_email = forms.EmailField(label= "ingresa de nuevo el mail: ")
	text = forms.CharField(widget=forms.Textarea)
	botcatcher = forms.CharField(required=False, widget = forms.HiddenInput)    
	#diversas formas de validar que el formulario es correcto
	# 1) no usar, haces la funcion manual para verificar que el campo oculto botcacher no ha sido manipuladp
	#1,1 creas elc ambpo bocachjer
	#def clean_botcatcher (self):
	#	botcacher = self.cleaned_data['botcatcher']
	#	if len(botcacher) > 0 :
	#		raise forms.ValidationError("Pillado bot")
	#	return botcacher

    #2 ) usar validators  (mismo caso de arriba con el catcher)

    # botcatcher = forms.CharField(required=False, widget = forms.HiddenInput
    #                              vlidators = [validators.MaxLengthValidator(0)])
	def clean(self):
		#savar los datos en crudo del proceso padre (formulario)
		all_clean_data = super().clean()
		email_clean = all_clean_data['email']
		vmail = all_clean_data['verify_email']
		if email_clean != vmail:
			print("esta entrando")
			raise forms.ValidationError("asegurate que los mails son iguales")