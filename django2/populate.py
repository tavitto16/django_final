import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','django2.settings')
import django
django.setup()


import random
from helper.models import AccessRecord, Topic, WebPage
from faker import Faker


#faken instance to create fake names
fakegen = Faker()


#default topic list
topics = ['Search','Social','MarketPlace','News','Games']

def add_topic():
	t = Topic.objects.get_or_create(topic_mame=random.choice(topics))[0]
	t.save()
	return t

def populate(N=20):
	for entry in range(N):

		#get a topic
		top = add_topic()

		# create fake data

		fake_url = fakegen.url()
		fake_name = fakegen.company()
		fake_date = fakegen.date()

		#create new entry for the website
		webpg = WebPage.objects.get_or_create(topic=top,url=fake_url,name=fake_name)[0]

		#create a fake access record for the webpage

		acess_record = AccessRecord.objects.get_or_create(name=webpg,date=fake_date)[0]


if __name__ == '__main__':
	print("starting population ...")
	populate()
	print("population complete")


