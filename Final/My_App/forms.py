from django import forms
from django.contrib.auth.models import User
from My_App.models import UserProfileInfo

class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())  # el widget permitira guarada la password sin que se vea
    # heredamos la clase de usuario por defecto de Django
	class Meta():
		model = User
		fields = ('username','email','password')    # asi incluimos solo los campos que queremos.  Tambien existe opcion __all__ para cogerlos todos
		# o tambien exioste la opcion exclude = ['param1','param2']

class UserProfileInfoForm(forms.ModelForm):
	#heredamos el modelo que hemos creado en models.py
	class Meta():
		model  = UserProfileInfo
		fields = ('portfolio_site','profile_pic')