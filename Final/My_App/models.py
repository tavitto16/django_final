from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class UserProfileInfo(models.Model):

	user = models.OneToOneField(User,on_delete=models.CASCADE)    #para evitar replicar usuarios creados. El perfil deriva del Modelo User

	#aditional classess
	portfolio_site = models.URLField(blank=True)   #puede no ser rellenada
	#subida de fotos
	profile_pic = models.ImageField(upload_to = 'profile_pics',blank=True)

	def __str__(self):
		return  self.user.username

