from django.shortcuts import render
from django.http import HttpResponse
from login_user.forms import NewUserForm
from login_user.models import User
# Create your views here.

def index(request):
	return render(request,"login_user/index.html")

def login_User(request):
	form = NewUserForm()
	if  request.method == 'POST':
		form  = NewUserForm(request.POST)   # post recoge los datos cuando se ha registrado un usuario

		if form.is_valid():
			form.save(commit=True)   # guardar datos si el formulario se ha rellenado correctamen e

			return index(request)   # volver a la pagina de inicio cuando un formulario se ha rellenado correctamente. 
		else: 
			print("ERROR!")
	return render(request,"login_user/create_users.html",{'form':form})

def users(request):
	users = User.objects.all()
	usuarios = {'users': users} 
	return render(request, 'login_user/users.html',context=usuarios)



