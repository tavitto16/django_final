from django.shortcuts import render
from django.http import HttpResponse
from helper.models import Topic, WebPage, AccessRecord
# Create your views here.



def helpme(request):
	wbpages_list = AccessRecord.objects.order_by('date')
	date_dict = {'access_records': wbpages_list} 
	return render(request, 'helper/help.html',context=date_dict)
