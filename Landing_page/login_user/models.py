from django.db import models

# Create your models here.

class User(models.Model):
	nombre = models.CharField(max_length=128)
	apellido = models.CharField(max_length=128)
	email = models.EmailField(max_length=256)
	