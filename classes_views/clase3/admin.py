from django.contrib import admin
from clase3.models import Escuela, Estudiante
# Register your models here.
admin.site.register(Escuela)
admin.site.register(Estudiante)