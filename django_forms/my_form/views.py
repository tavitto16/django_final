from django.shortcuts import render
from . import forms
# Create your views here.

def basic_form(request):
	return render(request,'my_form/index.html')

def basic_form_view(request):
	forumlario = forms.MyFormulario()

	if request.method == "POST":
		formu = forms.MyFormulario(request.POST)

		if formu.is_valid():
			#que hago con el codigo de momento imprimir
			print("recibiendo los datos del formulatio")
			print("Nombre: "+formu.cleaned_data["name"])
			print("Email: "+formu.cleaned_data["email"])
			print("Text: "+formu.cleaned_data["text"])

	return render(request, "my_form/formulario.html",{'form': forumlario})