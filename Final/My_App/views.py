from django.shortcuts import render
from My_App.forms import UserForm,UserProfileInfoForm
# Create your views here. ###################
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required

def index(request):
	return render(request,'My_App/index.html')


def register(request):
	registered = False

	if request.method == "POST": 
		user_form  = UserForm(data=request.POST)  #recuperamos los datos del formuladio
		profile_form = UserProfileInfoForm(data=request.POST)

		if user_form.is_valid() and profile_form.is_valid():
			#guardamos la informacion de registro e usuario
			user = user_form.save()
			user.set_password(user.password)
			user.save()

			#verificamos si la info del perfil es correcta
			profile = profile_form.save(commit=False)   
			profile.user = user  
			# si hay foro?
			if 'profile_pic' in request.FILES:
				profile.profile_pic = request.FILES['profile_pic']
			profile.save()
			registered = True
		else:
			print(user_form.errors,profile_form.errors)
	else:
		user_form = UserForm()
		profile_form = UserProfileInfoForm()
	print(registered)
	return render(request, 'My_App/registration.html', { 'user_form':user_form,
														'profile_form':profile_form,
														'registered':registered})

def user_login(request):
	if request.method == 'POST':
		#recuperar del formuolatio los campos username y passweord
		username = request.POST.get('username')
		password = request.POST.get('password')
		print(username,password)
		user = authenticate(request,username=username,password=password)
		print(str(user))
		if user:
			if user.is_active:
				login(request,user)
				return HttpResponseRedirect(reverse('index'))
			else:
				return HttpResponse("Cuenta no ACTIVA")
		else:
			print("Fallo de login")
			print("Username: {} and password {} ".format(username,password))
			return HttpResponse("Invalid logind details!!!!!")
	else:
		return render(request,'My_App/login.html')

# @ el login required es un decorador que hace que para que un usuario haga logout, necesariamente necesita necesita previamente estar loggeado
@login_required     
def user_logout(request):
	logout(request)
	return HttpResponseRedirect(reverse('index'))

# otro ejemplo:  Mostrar una pagina especial unicamne5te si se esta loggeado
@login_required
def special(request):
	return HttpResponse("efectivamente, estas loggeado")